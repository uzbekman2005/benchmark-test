package benchmarktest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFibonacci(t *testing.T) {
	testCases := []struct {
		input    int
		expected int
	}{
		{1, 1},
		{2, 1},
		{5, 5},
		{10, 55},
	}

	for _, testCase := range testCases {
		result := Fibonacci(testCase.input)
		assert.Equal(t, testCase.expected, result)
	}
}

func BenchmarkFibonacci(b *testing.B) {
	// sequences := []int{10, 20, 30}

	// for _, seq := range sequences {
	// 	b.Run(fmt.Sprintf("benchmark-sequence-%d", seq), func(b *testing.B) {
	// 		for i := 0; i < b.N; i++ {
	// 			Fibonacci(seq)
	// 		}
	// 	})
	// }
}
